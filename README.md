# DAF

Het DAF (Dynamisch Anoniem Fietsregister) heeft als doel om mensen die hun fiets kwijt zijn beter te informeren.
Hiertoe legt het een koppeling tussen (unieke) kenmerken op de fiets en gegevens van de fietseigenaar om hem te kunnen bereiken, zonder dat de privacy van de fietseigenaar in het geding is.

De gedachte van DAF is weergegeven in een [uitlegfilmpje](https://www.youtube.com/watch?v=q9kSvSEDQEo).

Allereerst wordt een MVP gerealiseerd bij voor fietsen die onderdeel van een vloot zijn (zoals lease en deelfietsen) de eigenaar/beheerder van de fiets geinformeerd wordt als een fiets bij een depot ingescaned wordt.

Zie verder [wiki](https://gitlab.com/DynamischAnoniemFietsregister/daf/wikis/home)

